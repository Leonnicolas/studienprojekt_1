# Studienprojekt_1

## 1) Installation Linux (Ubuntu & Debian)
1. Install Python 3.x.x from https://www.python.org/downloads/windows/
2. Install nodejs from https://nodejs.org/de/download/ and add it to the PathVariables
3. Clone the Repository 
    -> git clone https link for git Repo
4. Open the Project Folder E. g. with VsCode
5. Install npm
    -> sudo apt install npm
6. Install all necessary modules with npm using the comand
    -> npm -i
7. Install Pyhon_Modules usind pip
    1. NLTK
        -> pip install nltk 
    2. Pandas
        -> pip install pandas
    3. Textblob
        -> pip install textblob

## 2) Installation Windows 10
The commands are similar. Simply google the single commands for cmd

## 3) If npm -i doesn't install the necessary modules automatically
Install all necessary modules manually using npm
    1. index.js
        -> npm install index.js
    2. cors
        -> npm install cors
    3. d3-zoon
        -> npm install d3-zoom 
    4. express
        -> npm install express
    5. pip
        -> npm install pip
    6. pyhton-shell
        -> npm install pyhton-shell
    7. body-parser
        -> npm install body-parser
    8. file-system
        -> npm install file-system
    9. Pyhon_Modules usind pip as above 

## 4) Notes:
1. Test with name –version if modules or software are alrady installed
       e.g. node –version
2.  Maybe you have to use pip3 for pyhton3
       e.g. pip3 install nltk
3. Commands above works with Linux Debian and Ubuntu

## 5) Running

1. Open a Terminal, navigate into the Project Folder.
2. Start the Server -> node index.js
3. Open the HTML_Script.html File with a Browser.